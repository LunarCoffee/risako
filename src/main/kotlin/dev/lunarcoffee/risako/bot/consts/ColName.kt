package dev.lunarcoffee.risako.bot.consts

// Names of the MongoDB collections used.
internal object ColName {
    const val REMINDER = "Reminders0"
    const val TAGS = "Tags0"
    const val RPLACE_CANVAS = "RPlaceCanvas0"
    const val RPLACE_COOLDOWN = "RPlaceCooldown0"
    const val MUTE = "Mutes1"
    const val GUILD_OVERRIDES = "GuildOverrides0"
}
