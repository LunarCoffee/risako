package dev.lunarcoffee.risako.framework.core.annotations

internal annotation class CommandGroup(val name: String)
